# Remote Car Starter

SMS based remote car starter built on Arduino. Suitable for cars without remote control.

**WARNING!** Alpha version. Use at your own risk.

*Forked from: https://github.com/ben-jeffery/sms-remote-starter*

<br />

**SMS commands:**

| Basic | Setup |
| ------ | ------ |
|Init: Enable to save variables to the EEPROM.*|Dcontact+ : Increase contact delay with 250ms.|
|Start: Normal engine start.|Dcontact- : Decrease contact delay with 250ms.|
|Lstart: Long engine start.|Dstart+ : Increase start delay with 250ms.|
|Stop: Stop the engine.|Dstart- : Decrease start delay with 250ms.|
|Values: Show delay values.|Dlcontact+ : Increase long contact delay with 250ms.|
|Test: Test mobile network.|Dlcontact- : Decrease long contact delay with 250ms.|
|Rspon: Respond messages on.|Dlstart+ : Increase long start delay with 250ms.|
|Rspoff: Respond messages off.|Dlstart- : Decrease long start delay with 250ms.|

*\* Use **once** the "Init" command if you want to save new delay values. (Otherwise after restart the default values are restored.)*

<br />


**Necessary parts:**
- Arduino Uno R3. 
- SIM900 GSM Shield.
- Custom Shield (optional).
- 2 x Electromagnetic Car Relay (min. 40A/12V).
- DC-DC LM2596 Buck Converter (optional).

If you don't want to turn off the immobilizer in your car, you also need:
- An appropriate immobilizer antenna ring.
- A copy from your RFID transponder chip.
- SPDT Relay(6 pin) (eg. JQX-15F).

Custom shield part list:
- Arduino Protoshield.
- Screw Terminal Connector (min. 7 pin).
- 3 x 2N2222A or 2N3904 NPN Transistor.
- 4 x PC817 Optocoupler.
- 3 x 1N4448 Diode.
- 0.1 uF Film Capacitor.
- Resistors: 3 x 1KΩ/0.25W, 3 x 220Ω/0.25W, 10KΩ/0.25W, 1.8KΩ/1W, 330Ω/0.25W.

<br />

**Used Arduino Pins:** D5(Contact), D6(Start), D10(Oil pressure), D12(RFID antenna selector relay).

**Required Libraries:**
[Rocketscream Low Power Library,](https://github.com/rocketscream/Low-Power)
[Built-in SoftwareSerial Library,](https://www.arduino.cc/en/Reference/SoftwareSerial)
[Built-in EEPROM Library](https://www.arduino.cc/en/Reference/EEPROM)

**Usefull links:** [SIM900 GPRS Shield](https://randomnerdtutorials.com/sim900-gsm-gprs-shield-arduino/)

**License:** The source code is under GPLv3.