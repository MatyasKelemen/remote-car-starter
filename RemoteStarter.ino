/*

  SMS REMOTE CAR STARTER
  Forked from: https://github.com/ben-jeffery/sms-remote-starter
  Modified for general use (for cars without remote control).
  License: GPLv3

  https://gitlab.com/MatyasKelemen

*/


#include "LowPower.h"
#include <EEPROM.h>
#include <SoftwareSerial.h>

// SIM900 Shield Serial Pins.
SoftwareSerial SIM900(7, 8);

// Set to true if you want confirmation text messages.
boolean respond = true;

// Start test result.
boolean engine_running = false;
// Oil pressure presence.
boolean oil_pressure = false;

// Pins
int contactPin = 5; // ACC circuit.
int starterPin = 6; // Ignition circuit.
int sensorPinIn = 10; // Oil pressure sensor.
int rfidPin = 12; // Relay for two RFID antenna. (Switch between factory and secondary immobilizer antenna ring.)

//
int CONTACT_DELAY = 2000; // ACC ON, then start the engine after two seconds.
int START_DELAY = 750; // Self starter ON for 0.75 seconds.
int LONG_CONTACT_DELAY = 3000; // ACC ON, then start the engine after three seconds.
int LONG_START_DELAY = 1250; // Self starter ON for 1.25 seconds.

// Replace with the number of the controlling phone.
String myPhoneNum = "+xxxxxxxxxxx";
// Last modified delay value.
String actual_delay = "";
// Gear reminder.
String start_confirm = "";
String lstart_confirm = "";
// Count failed attempts to connect to the mobile network.
int counter = 0;


void setup()
{
  pinMode(9, OUTPUT); // SIM900 power on pin.
  pinMode(13, OUTPUT); // Built-in LED.
  pinMode(rfidPin, OUTPUT);
  pinMode(starterPin, OUTPUT);
  pinMode(contactPin, OUTPUT);
  pinMode(sensorPinIn, INPUT);

  Serial.begin(9600); // Start serial monitor.
  Serial.println("Starting...");
  SIM900.begin(9600); // Start serial connection between GSM shield and Arduino.
  SIM900poweron();  // Turn on GSM shield.

  //Copy data from EEPROM.
  if (EEPROM.read(0) != 0) {
    CONTACT_DELAY = EEPROMReadInt(0);
    LONG_CONTACT_DELAY = EEPROMReadInt(2);
    START_DELAY = EEPROMReadInt(4);
    LONG_START_DELAY = EEPROMReadInt(6);
    Serial.println("Reading from EEPROM...");
  }

  // Wake up modem with an AT command.
  sendCommand("AT", "OK", 1000);
  if (sendCommand("AT", "OK", 1000) == 1) {
    Serial.println("Module started.");
  }
  if (sendCommand("AT+IPR=9600", "OK", 1000) == 1) {
    Serial.println("Baud rate is set to: 9600");
  }
  if (sendCommand("AT+CNMI=0,0,0,0,0", "OK", 1000) == 1) {
    Serial.println("Notifications disabled.");
  }
  if (sendCommand("AT+CMGF=1", "OK", 1000) == 1) {
    Serial.println("Text mode enabled.");
  }
  if (sendCommand("AT+CCLK?", "+CCLK: ", 1000) == 1) {
    Serial.println("Time query: OK");
  }
  if (sendCommand("AT+CSCLK=2", "OK", 1000) == 1) {
    Serial.println("Sleeping enabled.");
  }
}



void loop()
{
  // Wake up modem with two AT commands.
  sendCommand("AT", "OK", 1000);
  sendCommand("AT", "OK", 2000);
  // Check if it's currently registered to network.
  if (sendCommand("AT+CREG?", "+CREG: 1,1", 1000)) {

    Serial.println("Connected to the home cell network");

    // Try to get the first SMS. Reply prefixed with "+CMGR:" (if there's a new SMS).
    if (sendCommand("AT+CMGR=1", "+CMGR: ", 1000) == 1) {
      String serialContent = "";
      char serialCharacter;

      if (SIM900.isListening()) {
        Serial.println("Port is listening!");

        while (SIM900.available()) {
          serialCharacter = SIM900.read();
          serialContent.concat(serialCharacter);
          delay(10);
        }
      }

      // Dividing up the new SMS.
      String smsNumber = serialContent.substring(14, 26);
      String smsMessage = serialContent.substring(54, serialContent.length() - 7);
      smsMessage.trim();
      smsMessage.toLowerCase();

      Serial.println("New SMS Message");
      Serial.println(smsNumber);
      Serial.println(smsMessage);

      // Delete all SMS messages in memory.
      sendCommand("AT+CMGD=1,4", "OK", 1000);

      // Check if it's coming from my phone.
      if (smsNumber == myPhoneNum) {
        if (smsMessage == "start") {
          start_confirm = "ok";
          if (respond) {
            sendSms("Did you take the car out of gear?");
          }
        }
        else if (smsMessage == "yes" && start_confirm == "ok") {
          start_confirm = "";
          startTest();
          if (engine_running) {
            Serial.println("The engine is already ON.");
            if (respond) {
              sendSms("The engine is already ON.");
            }
          }
          else {
            carStart();
            startTest();
            if (engine_running) {
              Serial.println("Successful start.");
              if (respond) {
                sendSms("Successful start.");
              }
            }
            else {
              Serial.println("Start failed.");
              if (respond) {
                sendSms("Start failed.");
              }
            }
          }
        }
        else if (smsMessage == "lstart") {
          lstart_confirm = "ok";
          if (respond) {
            sendSms("Did you take the car out of gear?");
          }
        }
        else if (smsMessage == "yes" && lstart_confirm == "ok") {
          lstart_confirm = "";
          startTest();
          if (engine_running) {
            Serial.println("The engine is already ON.");
            if (respond) {
              sendSms("The engine is already ON.");
            }
          }
          else {
            longStart();
            startTest();
            if (engine_running) {
              Serial.println("Successful start.");
              if (respond) {
                sendSms("Successful start.");
              }
            }
            else {
              Serial.println("Start failed.");
              if (respond) {
                sendSms("Start failed.");
              }
            }
          }
        }
        else if (smsMessage == "stop") {
          carStop();
          startTest();
          if (!engine_running) {
            Serial.println("Engine stopped.");
            if (respond) {
              sendSms("Engine stopped.");
            }
          }
          else {
            Serial.println("Engine didn't stopped!");
            if (respond) {
              sendSms("Engine didn't stopped!");
            }
          }
        }
        else if (smsMessage == "rspon") {
          respond = true;
          sendSms("Respond to commands: On");
        }
        else if (smsMessage == "rspoff") {
          respond = false;
          sendSms("Respond to commands: Off");
        }
        else if (smsMessage == "test") {
          sendSms("Test: OK");
        }
        else if (smsMessage.startsWith("d")) {
          delayModifier(smsMessage);
          if (respond) {
            sendSms(actual_delay);
          }
        }
        else if (smsMessage == "init") {
          defaultEEPROM();
          if (respond) {
            sendSms("Default EEPROM values are set.");
          }
        }
        else if (smsMessage == "values") {
          if (respond) {
            sendSms(delayValues());
          }
        }
        else {
          if (respond) {
            sendSms("Invalid command");
          }
          Serial.println("Invalid command");
        }
      }
    }
    else {
      Serial.println("No SMS messages");
    }
  }
  else {
    Serial.println("Not connected to home cell network");
    counter += 1;
    if (counter >= 5) {
      SIM900poweron(); // After five failed connection attempt, start again the GSM module.
      counter = 0;
      delay(500);
    }
  }


  delay(500);
  LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);

}


void carStart() {
  Serial.println("Starting...");
  digitalWrite(rfidPin, HIGH); // Switch to the secondary immobilizer antenna with built-in immobilizer chip. (You need a copy from your RFID transponder.)
  digitalWrite(contactPin, HIGH); // Turn on ACC circuit.
  delay(CONTACT_DELAY); // Wait before start. (Injector heating time.)
  digitalWrite(starterPin, HIGH); // Turn on the self-starter.
  delay(START_DELAY); // Ignition time.
  digitalWrite(starterPin, LOW); // Turn off the self-starter.
}


void carStop() {
  Serial.println("Stopping...");
  digitalWrite(starterPin, LOW);
  delay(300);
  digitalWrite(contactPin, LOW);
  digitalWrite(rfidPin, LOW);
}


void longStart() {
  Serial.println("Long starting...");
  digitalWrite(rfidPin, HIGH);
  digitalWrite(contactPin, HIGH);
  delay(LONG_CONTACT_DELAY);
  digitalWrite(starterPin, HIGH);
  delay(LONG_START_DELAY);
  digitalWrite(starterPin, LOW);
}


void startTest()
// If there is oil pressure the engine is on.
{
  delay(30000);
  oil_pressure = (digitalRead(sensorPinIn) == HIGH); // The optocoupler is turning on if the oil pressure sensor makes ground connection.
  if (oil_pressure) {
    Serial.println("There is oil pressure.");
    engine_running = true;
  }
  else {
    Serial.println("No oil pressure.");
    engine_running = false;
    carStop();
  }
  oil_pressure = false;
  digitalWrite(rfidPin, LOW);
}


void delayModifier(String message)
// Adjust injector heating or ignition time.
{
  message.remove(0, 1);
  Serial.println(message);
  if (message.startsWith("contact+")) {
    if (CONTACT_DELAY < 4000) {
      CONTACT_DELAY += 250;
      EEPROMWriteInt(0, CONTACT_DELAY);
    }
    actual_delay = String(CONTACT_DELAY);
    Serial.println(actual_delay);
  }
  else if (message.startsWith("contact-")) {
    if (CONTACT_DELAY >= 500) {
      CONTACT_DELAY -= 250;
      EEPROMWriteInt(0, CONTACT_DELAY);
    }
    actual_delay = String(CONTACT_DELAY);
    Serial.println(actual_delay);
  }
  else if (message.startsWith("lcontact+")) {
    if (LONG_CONTACT_DELAY < 6000) {
      LONG_CONTACT_DELAY += 250;
      EEPROMWriteInt(2, LONG_CONTACT_DELAY);
    }
    actual_delay = String(LONG_CONTACT_DELAY);
    Serial.println(actual_delay);
  }
  else if (message.startsWith("lcontact-")) {
    if (LONG_CONTACT_DELAY >= 1000) {
      LONG_CONTACT_DELAY -= 250;
      EEPROMWriteInt(2, LONG_CONTACT_DELAY);
    }
    actual_delay = String(LONG_CONTACT_DELAY);
    Serial.println(actual_delay);
  }
  else if (message.startsWith("start+")) {
    if (START_DELAY < 4000) {
      START_DELAY += 250;
      EEPROMWriteInt(4, START_DELAY);
    }
    actual_delay = String(START_DELAY);
    Serial.println(actual_delay);
  }
  else if (message.startsWith("start-")) {
    if (START_DELAY >= 500) {
      START_DELAY -= 250;
      EEPROMWriteInt(4, START_DELAY);
    }
    actual_delay = String(START_DELAY);
    Serial.println(actual_delay);
  }
  else if (message.startsWith("lstart+")) {
    if (LONG_START_DELAY < 6000) {
      LONG_START_DELAY += 250;
      EEPROMWriteInt(6, LONG_START_DELAY);
    }
    actual_delay = String(LONG_START_DELAY);
    Serial.println(actual_delay);
  }
  else if (message.startsWith("lstart-")) {
    if (LONG_START_DELAY >= 1000) {
      LONG_START_DELAY -= 250;
      EEPROMWriteInt(6, LONG_START_DELAY);
    }
    actual_delay = String(LONG_START_DELAY);
    Serial.println(actual_delay);
  }
  else {
    Serial.println("Invalid delay modifier SMS.");
    actual_delay = "Invalid delay command.";
  }
}


void sendSms(String message)
// Sending SMS with AT commands.
{
  Serial.println("Sending SMS...");
  SIM900.println("AT+CMGF=1");
  delay(1000);
  SIM900.println("AT+CMGS=\"" + myPhoneNum + "\"");
  delay(1000);
  SIM900.println(message);
  delay(100);
  SIM900.println((char)26); // ASCII code of CTRL+Z
  delay(3000);
}


void SIM900poweron()
// Software equivalent of pressing the GSM shield "power" button.
{
  //Wake up modem with an AT command
  sendCommand("AT", "OK", 1000);
  if (sendCommand("AT", "OK", 2000) == 0) {
    digitalWrite(9, HIGH);
    delay(1000);
    digitalWrite(9, LOW);
    delay(7000);
  }
}


void EEPROMWriteInt(int p_address, int p_value)
// Store one integer number in two EEPROM address. (Only one is not enough.)
{
  byte lowByte = ((p_value >> 0) & 0xFF);
  byte highByte = ((p_value >> 8) & 0xFF);

  EEPROM.put(p_address, lowByte);
  EEPROM.put(p_address + 1, highByte);
}


unsigned int EEPROMReadInt(int p_address)
// Read one integer number from two EEPROM address.
{
  byte lowByte = EEPROM.read(p_address);
  byte highByte = EEPROM.read(p_address + 1);

  return ((lowByte << 0) & 0xFF) + ((highByte << 8) & 0xFF00);
}


void defaultEEPROM()
// Wipe out the EEPROM and save in it the default delay values.
{
  for (int i = 0 ; i < EEPROM.length() ; i++) {
    EEPROM.write(i, 0);
  }

  EEPROMWriteInt(0, 2000);
  EEPROMWriteInt(2, 3000);
  EEPROMWriteInt(4, 750);
  EEPROMWriteInt(6, 1250);

  Serial.println("Default delay values copied to EEPROM.");
}


String delayValues()
// Compose the values response sms.
{
  String values = "Contact: " + String(CONTACT_DELAY) + "\nStart: " + String(START_DELAY) + "\nLcontact: " + String(LONG_CONTACT_DELAY) + "\nLstart: " + String(LONG_START_DELAY);
  return values;
}


int sendCommand(const char* ATcommand, const char* expected_answer, unsigned int timeout)
// Send AT command to the GSM module, and check the respond. If the output and the expected answer is equal the function return with 1, otherwise return with 0.
{

  int answer = 0;
  int responsePos = 0;
  char response[100];
  unsigned long previous;

  memset(response, '\0', 100);    // Clears array.

  delay(100);

  while ( SIM900.available() > 0) SIM900.read();   // Clean the input buffer.

  SIM900.println(ATcommand);    // Send the AT command.


  responsePos = 0;
  previous = millis();

  // this loop waits for the answer
  do {
    // if there are data in the UART input buffer, reads it and checks for the answer.
    if (SIM900.available() != 0) {
      response[responsePos] = SIM900.read();
      responsePos++;
      // check if the desired answer is in the response of the module.
      if (strstr(response, expected_answer) != NULL)
      {
        answer = 1;
      }
    }
    // Waits for the asnwer with time out.
  } while ((answer == 0) && ((millis() - previous) < timeout));
  return answer;
}

